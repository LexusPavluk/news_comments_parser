package com.gmail.lexuspavluk.news_comments_parser;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class NewsCommentsParserApplication {

    public static void main(String[] args) {
        SpringApplication.run(NewsCommentsParserApplication.class, args);
    }

}
